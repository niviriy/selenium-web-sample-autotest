package org.example;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;

public class Test100Button {
    @Test
    public void testButton() {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium-web-sample\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        JavascriptExecutor js = (JavascriptExecutor) driver;

        driver.get("C:\\selenium-web-sample-autotese\\src\\test\\resources\\test100Btn.html");

        int i = 1;
        for (i = 1; i <= 100; i++) {
            WebElement btn = driver.findElement(By.id("btn" + i));
            String nameBtn = driver.findElement(By.id("btn" + i)).getAttribute("value");
            System.out.println(nameBtn);
            if (nameBtn.equals("100")) {

                js.executeScript("arguments[0].scrollIntoView();", btn);
            }
        }
    }
        //driver.close();

}